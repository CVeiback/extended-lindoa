classdef Presentation < handle  
  properties (SetAccess = private)
    responses
    scenario
    names
    colors
  end
  
  methods
    function m = Presentation(scenario, response, name)      
      m.scenario = scenario;
      if nargin < 2
        m.responses = [];
        m.names = {};
      elseif nargin < 3
        m.AddResponse(response)
      else
        m.AddResponse(response, name);
      end      
      m.SetupColors();
    end
    
    function SetupColors(m, setRoot)
      m.colors = load('colors');
      if nargin >= 2 && setRoot
        set(groot, 'DefaultAxesColorOrder', m.colors.plotColors);
        set(groot,'DefaultFigureColormap', m.colors.mapColors);
      end
    end
    
    function AddResponse(m, response, name)
      if nargin < 3 || isempty(name)
        m.names{end+1} = response.algorithm;
      else
        m.names{end+1} = name;
      end
      if isempty(m.responses)
        m.responses = response;
      else
        m.responses(end+1) = response;
      end
    end
    
    function methodErrors = EstimationError(m)
      numResponses = length(m.responses);
      methodErrors = zeros(1, numResponses);
      trueDirections = m.GetTrueDirections();
      for ii = 1:numResponses  
        [~, maxIndex] = max(m.responses(ii).response);
        directions = [m.responses(ii).locations.location];
        
        estimate = directions(maxIndex);
        
        err = estimate - trueDirections;
        err = mod(err + pi, 2 * pi) - pi;
        err = min(abs(err));
        methodErrors(ii) = sqrt(mean(err.^2));
      end
    end
    
    function trueDirections = GetTrueDirections(m)
      numSources = length(m.scenario.sources);
      trueDirections = zeros(numSources,1);
      for ii = 1:numSources
        trueDirections(ii) = m.scenario.sources(ii).location.location;
      end
    end
    
    function PrintError(m)
      methodErrors = m.EstimationError();
      numResponses = length(methodErrors);
      for ii = 1:numResponses
        disp([m.names{ii} ': ' num2str(round(methodErrors(ii),2))])
      end
    end
    
    function DoAImage(m)
      for ii = 1:length(m.responses)        
        response = m.Normalize(m.responses(ii).response);
        directions = [m.responses(ii).locations.location];
        timestamps = m.responses(ii).timestamps;

        figure;
        m.DrawImage(timestamps, rad2deg(directions), response)
        colormap(gcf, m.colors.mapColors)
        
        hold on;
        trueDirections = mod(m.GetTrueDirections(), 2*pi);
        plot3(timestamps, rad2deg(trueDirections) .* ones(size(timestamps)), ones(size(timestamps)), 'Color', 0.3*[1 1 1]);
        
        title(m.names{ii})
        ylabel('Direction of Arrival (deg)');
        xlabel('Time (s)');
      end
    end
    
    function DrawImage(~, x, y, values)
      [X,Y] = meshgrid(x, y);
      surf(X, Y, values, 'EdgeColor', 'none');
      view(2);
      
      ylim(([min(y) max(y)]));
      xlim([min(x), max(x)]);
    end
    
    function normalizedResponse = Normalize(~, response)
      [minResponse, maxResponse] = bounds(response);
      rangeResponse = maxResponse - minResponse;
      normalizedResponse = (response - minResponse) ./ rangeResponse;
    end
  end
end

