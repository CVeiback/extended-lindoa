close all
clearvars

%% Setup
addpath(genpath('.'));
[scenario, array] = CreateExperimentScenario();
[directions, frameProcessor] = SetupEstimationFramework();

time = 2;
recording = scenario.Simulate(time);

%% Regression Test SRP
srpBenchmark = SRPBenchmark(array, directions);
srpMethod = SteeredResponsePower(array, directions);
Benchmark(srpBenchmark, srpMethod, frameProcessor, recording);

%% Regression Test MVDR
mvdrBenchmark = MPDRBenchmark(array, directions);
mvdrMethod = MinimumVarianceDistortionlessResponse(array, directions);
Benchmark(mvdrBenchmark, mvdrMethod, frameProcessor, recording);

%% Regression Test LinDoA
parameters = LinDoATestParameters();
numParameters = length(parameters);

timeBenchmark = zeros(1, numParameters);
timeMethod = zeros(1, numParameters);
totalError = zeros(1, numParameters);
for ii = 1:numParameters
  pars = parameters{ii};
  lindoaBenchmark = LinDoABenchmark(array, directions, pars{:});
  lindoaMethod = LinDoA(array, directions, pars{:});
  [timeBenchmark(ii), timeMethod(ii), totalError(ii)]  = ...
    Benchmark(lindoaBenchmark, lindoaMethod, frameProcessor, recording);
end

disp(['Benchmark Time: ' num2str(round(timeBenchmark, 2)) ' s'])
disp(['Method Time:    ' num2str(round(timeMethod,2)) ' s'])
disp(['Total Error:    ' num2str(round(totalError,2, 'significant'))])

errorTolerance = 1e-5;
failedError = find(totalError > errorTolerance);
if isempty(failedError)
  disp('All tests are within error tolerance.');
else
  disp(['Failed error: ' num2str(failedError)]);
end

timeToleranceFactor = 2;
failedTime = find(timeMethod > timeToleranceFactor * timeBenchmark);
if isempty(failedTime)
  disp('All tests are within time tolerance.');
else
  disp(['Failed time: ' num2str(failedTime)]);
end

%% Various combinations of test parameters for LinDoA
function parameters = LinDoATestParameters()
  order = 5;
  forgettingFactor = 0.1;
  ordinary = 0;
  local = true;
  nonLocal = false;
  numSelected = 7;
  average = true;
  standard = false;

  parameters = {};
  parameters{end+1} = {order, forgettingFactor, local, numSelected, standard, 'DiscretePower'};
  parameters{end+1} = {order, forgettingFactor, local, numSelected, average, 'DiscretePower'};
  parameters{end+1} = {order, ordinary, local, numSelected, standard, 'DiscretePower'};
  parameters{end+1} = {order, ordinary, local, numSelected, average, 'DiscretePower'};
  parameters{end+1} = {order, forgettingFactor, local, numSelected, standard, 'ContinuousPower'};
  parameters{end+1} = {order, forgettingFactor, local, numSelected, standard, 'Error'};
  parameters{end+1} = {order, forgettingFactor, nonLocal, numSelected, standard, 'DiscretePower'};
  parameters{end+1} = {order+1, forgettingFactor, local, numSelected, standard, 'DiscretePower'};
  parameters{end+1} = {order+1, forgettingFactor, local, numSelected, average, 'DiscretePower'};
  parameters{end+1} = {order, forgettingFactor, local, numSelected+1, standard, 'DiscretePower'};
  parameters{end+1} = {order, forgettingFactor, local, numSelected+1, average, 'DiscretePower'};
end

%%
function [timeBenchmark, timeMethod, totalError] = Benchmark(benchmark, method, frameProcessor, recording)
  tic
  responseBenchmark = frameProcessor.ComputePower(recording', benchmark);
  timeBenchmark = toc;

  tic
  response = frameProcessor.ComputePower(recording', method);
  timeMethod = toc;

  err = responseBenchmark.response - response.response;
  totalError = sum(abs(err(:)));

  if nargout == 0
    disp(['Benchmark Time: ' num2str(timeBenchmark) ' s'])
    disp(['Method Time:    ' num2str(timeMethod) ' s'])
    disp(['Total Error:    ' num2str(totalError)])
  end
end

%% 
function [directions, frameProcessor] = SetupEstimationFramework()
numDirections = 360;
frameSize = 1024;

directionGrid = linspace(0, 2*pi, numDirections + 1); directionGrid(end) = [];
directions = Direction.Generate(directionGrid);

frameProcessor = FrameProcessor(frameSize);
end