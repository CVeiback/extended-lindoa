function [scenario, array] = CreateExperimentScenario()
numElements = 8;
arrayRadius = 0.1;
arrayOrientation = deg2rad(0);
noise = 1;
samplingFrequency = 8000;

sourceDirection = Direction.Generate(deg2rad([45 -90]));

array = CircularArray(numElements, arrayRadius, arrayOrientation, noise, samplingFrequency);
scenario = Scenario(array);

for ii = 1:length(sourceDirection)
  signal = SoundFile(ii);
  source = Source(signal, sourceDirection(ii));
  scenario.AddSource(source);
end
end

