classdef Response < handle
  
  properties (SetAccess = private)
    response
    locations
    algorithm
    timestamps
  end
  
  methods
    function m = Response(response, algorithm, timestamps)
      m.response = response;  
      m.locations = algorithm.locations;
      m.algorithm = class(algorithm);
      m.timestamps = timestamps;
    end
  end
end

