classdef Delays < handle
  
  properties (SetAccess = private)
    delays
    maxDelay
    numSelected
    
    selection
  end
  
  methods
    function m = Delays(delays, maxDelay, numSelected)
      m.delays = delays;
      m.maxDelay = maxDelay;
      m.numSelected = numSelected;
      
      m.InitAllSelections();
    end
    
    function stackedSelection = Stacked(m, location)
      stackedSelection = reshape(m.selection(:, :, location), [], 1);
    end
    
    function selection = SingleElement(m, element, location)
      selection = m.selection(:, element, location);
    end
    
    function selection = SingleElementAsStacked(m, element, location)
      elementSelection = m.selection(:, element, location);              
      stackedIndex = find(elementSelection) + (element-1) * m.NumDelays();
      selection = false(m.NumDelays() * m.NumElements(), 1);
      selection(stackedIndex) = true;
    end
    
    function selectedDelays = SelectedDelays(m, element, location)
      rangeOfDelays = m.delays(element, location) - (-m.maxDelay:m.maxDelay);
      selectedDelays = rangeOfDelays(m.selection(:, element, location));
    end
    
    function maxDelay = GetMaxDelay(m)
      maxDelay = m.maxDelay;
    end
    
    function numSelected = NumSelected(m)
      numSelected = m.numSelected;
    end
        
    function numLocations = NumLocations(m)
      numLocations = size(m.delays, 2);
    end
    
    function numElements = NumElements(m)
      numElements = size(m.delays, 1);
    end
    
    function numDelays = NumDelays(m)
      numDelays = 2 * m.maxDelay + 1;
    end    
  end
    
  methods (Access = private)
    function InitAllSelections(m)      
      m.selection = false(m.NumDelays(), m.NumElements(), m.NumLocations());      
      for location = 1:m.NumLocations()
        for element = 1:m.NumElements()
          m.InitSelection(element, location);
        end
      end  
    end
    
    function InitSelection(m, element, location)
      rangeOfDelays = m.delays(element, location) - (-m.maxDelay:m.maxDelay);      
      selectedDelays = m.SelectSmallestDelays(rangeOfDelays);
      m.selection(:, element, location) = selectedDelays;
    end
    
    function selectedDelays = SelectSmallestDelays(m, rangeOfDelays)
      [~, selectedIndex] = sort(abs(rangeOfDelays));
      selectedIndex = selectedIndex(1:m.numSelected);
      selectedDelays = false(m.NumDelays(), 1);
      selectedDelays(selectedIndex) = true;
    end
  end
end

