classdef AverageFilter < Filter
  
  methods (Access = protected)
    function InitFilter(m)
      m.InitEmptyFilterMatrix();
      for location = 1:m.NumLocations()
        for element = 1:m.NumElements()
          m.InitSingleFilter(element, location);
        end
      end
    end
    
    function InitSingleFilter(m, element, location)      
      singleObservationMatrix = m.observationMatrix.GetForElement(element, location);
      stackedSelection = m.selectedDelays.SingleElementAsStacked(element, location);
      filter = pinv(singleObservationMatrix) / m.NumElements(); 
      m.filterMatrix(stackedSelection, :, location) = filter;
    end
  end
end

