classdef MinimumVarianceAverageFilter < Filter
  
  properties
    covariance
    forgettingFactor
    
    interpolationFilterMatrix
  end
  
  methods
    function m = MinimumVarianceAverageFilter(observationMatrix, ...
        selectedDelays, discreteEstimate, forgettingFactor)
      m@Filter(observationMatrix, selectedDelays, discreteEstimate)
      
      m.forgettingFactor = forgettingFactor;
      m.InitCovariance();
    end
  end
  
  methods (Access = protected)  
    function InitFilter(m)
      m.ComputeInterpolationFilters();
    end
    
    function ComputeInterpolationFilters(m)   
      numTotalSelected = m.NumElements() * m.NumSelected();
      m.interpolationFilterMatrix = zeros(numTotalSelected, m.NumElements(), m.NumLocations());      
      for location = 1:m.NumLocations()
        for element = 1:m.NumElements()
          m.ComputeSingleInterpolationFilter(element, location);
        end
      end
    end
    
    function ComputeSingleInterpolationFilter(m, element, location)
      selectFirstState = [1; zeros(m.NumStates() - 1, 1)];
      elementIndex = (element - 1) * m.NumSelected() + (1:m.NumSelected());
      singleObservationMatrix = m.observationMatrix.GetForElement(element, location);
      m.interpolationFilterMatrix(elementIndex, element, location) = ...
        ComputeFilter(singleObservationMatrix, selectFirstState);
      
      function filter = ComputeFilter(H, u)
        filter = H' * ((H * H') \ u);
      end
    end
    
    function InitCovariance(m)
      m.covariance = eye(m.NumElements() * m.NumDelays());      
    end
    
    function UpdateFilter(m, stackedSignal)
      m.UpdateCovariance(stackedSignal);
      m.UpdateFilterMatrix();
    end
    
    function UpdateCovariance(m, stackedSignal)
      numSamples = size(stackedSignal, 1);
      covarianceUpdate = stackedSignal' * stackedSignal / numSamples;
      m.covariance = (1-m.forgettingFactor) * m.covariance + ...
        m.forgettingFactor * covarianceUpdate ;
    end
    
    function UpdateFilterMatrix(m)
      assert(m.discreteEstimate, 'Average Minimum Variance LinDoA only supports discrete estimates.')
      m.InitEmptyFilterMatrix();
      for location = 1:m.NumLocations()
        selected = m.selectedDelays.Stacked(location);
        selectedCovariance = m.covariance(selected, selected);
        selectedInterpolation = sparse(m.interpolationFilterMatrix(:,:,location));
        filter = ComputeFilter(selectedInterpolation, selectedCovariance);
        m.filterMatrix(selected, 1, location) = filter;
      end
      
      function filter = ComputeFilter(V, R)
        one = ones(size(V, 2), 1);
        unscaledFilter = (V' * R * V) \ one; 
        scaleFactor = one' * unscaledFilter;
        scaledFilter = unscaledFilter / scaleFactor;
        filter = V * scaledFilter;
      end
    end
  end
end

