classdef MinimumVarianceStandardFilter < Filter
  
  properties
    covariance
    forgettingFactor
  end
  
  methods
    function m = MinimumVarianceStandardFilter(observationMatrix, ...
        selectedDelays, discreteEstimate, forgettingFactor)
      m@Filter(observationMatrix, selectedDelays, discreteEstimate)
      
      m.forgettingFactor = forgettingFactor;
      m.InitCovariance();
    end
  end
  
  methods (Access = protected)  
    function InitCovariance(m)
      m.covariance = eye(m.NumElements() * m.NumDelays());      
    end
    
    function UpdateFilter(m, stackedSignal)
      m.UpdateCovariance(stackedSignal);
      m.UpdateFilterMatrix();
    end
    
    function UpdateCovariance(m, stackedSignal)
      numSamples = size(stackedSignal, 1);
      covarianceUpdate = stackedSignal' * stackedSignal / numSamples;
      m.covariance = (1-m.forgettingFactor) * m.covariance + ...
        m.forgettingFactor * covarianceUpdate ;
    end
    
    function UpdateFilterMatrix(m)
      m.InitEmptyFilterMatrix();
      for location = 1:m.NumLocations()
        selected = m.selectedDelays.Stacked(location);
        selectedObservations = m.observationMatrix.GetFullForLocation(location);
        selectedCovariance = m.covariance(selected, selected);
        filter = ComputeFilter(selectedObservations, selectedCovariance);
        m.filterMatrix(selected, :, location) = filter;
      end
      function filter = ComputeFilter(H, R)
        RH = R \ H';
        HRH = H * RH;
        filter = RH / HRH;
      end
    end
  end
end

