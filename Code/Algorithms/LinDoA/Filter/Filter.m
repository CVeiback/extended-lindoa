classdef Filter < handle
  
  properties
    observationMatrix
    selectedDelays  
    discreteEstimate
    filterMatrix
  end
  
  methods
    function m = Filter(observationMatrix, selectedDelays, discreteEstimate)
      m.observationMatrix = observationMatrix;
      m.selectedDelays = selectedDelays;
      m.discreteEstimate = discreteEstimate;
      m.InitFilter();
    end
    
    function filteredSignal = UpdateAndFilter(m, stackedSignal)
      m.UpdateFilter(stackedSignal);
      m.DiscretizeFilterIfNecessary();
      filteredSignal = m.FilterSignal(stackedSignal);
    end
    
    function numLocations = NumLocations(m)
      numLocations = m.observationMatrix.NumLocations();
    end
    
    function numElements = NumElements(m)
      numElements = m.observationMatrix.NumElements();
    end
    
    function numStates = NumStates(m)
      numStates = m.observationMatrix.NumStates();
    end
    
    function numDelays = NumDelays(m)
      numDelays = m.observationMatrix.NumDelays();
    end
    
    function numSelected = NumSelected(m)
      numSelected = m.observationMatrix.NumSelected();
    end
  end
  
  methods (Access = protected)
    function InitFilter(m)
      m.InitEmptyFilterMatrix();
    end
    
    function InitEmptyFilterMatrix(m)
      m.filterMatrix = zeros(m.NumElements() * m.NumDelays(), m.NumStates(), m.NumLocations());
    end
    
    function UpdateFilter(~, ~)
      
    end
    
    function DiscretizeFilterIfNecessary(m)
      if m.NeedsToBeDiscretized()
        m.DiscretizeFilter();
      end
    end
    
    function needsToBeDiscretized = NeedsToBeDiscretized(m)
      isDiscrete = size(m.filterMatrix, 3) == 1;
      needsToBeDiscretized = m.discreteEstimate && ~isDiscrete;
    end
    
    function DiscretizeFilter(m)
      m.filterMatrix = reshape(m.filterMatrix(:,1,:), [], m.NumLocations());
    end
    
    function filteredSignal = FilterSignal(m, signal)
      filteredSignal = pagemtimes(signal, m.filterMatrix);
    end
  end
end

