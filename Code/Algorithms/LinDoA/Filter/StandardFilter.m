classdef StandardFilter < Filter
  
  methods (Access = protected)
    function InitFilter(m)
      m.InitEmptyFilterMatrix();
      for location = 1:m.NumLocations()
        m.InitFilterForLocation(location);
      end
    end
    
    function InitFilterForLocation(m, location)
        selected = m.selectedDelays.Stacked(location);
        selectedObservations = m.observationMatrix.GetFullForLocation(location);
        filter = pinv(selectedObservations);
        m.filterMatrix(selected, :, location) = filter;
    end
  end
end

