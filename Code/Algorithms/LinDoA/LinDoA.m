classdef LinDoA < Algorithm
  
  properties (SetAccess = private)      
    observationMatrix
    selectedDelays    
    filter
    evaluation
    signalStacker
    
    stackedSignal
    filteredSignal
  end
  
  methods
    function m = LinDoA (array, locations, order, forgettingFactor, ...
        local, interpolationSamples, average, evaluationMethod)
      if nargin < 3 || isempty(order)
        order = 5;
      end
      if nargin < 4 || isempty(forgettingFactor)
        forgettingFactor = 0.1;
      end
      if nargin < 5 || isempty(local)
        local = true;
      end
      if nargin < 6 || isempty(interpolationSamples)
        interpolationSamples = 7;
      end
      if nargin < 7 || isempty(average)
        average = false;
      end
      if nargin < 8 || isempty(evaluationMethod)
        evaluationMethod = 'discretepower';
      end
            
      m@Algorithm(array, locations);
      
      maxDelay = m.ComputeMaxDelay(interpolationSamples);
      numSelected = m.ComputeNumSelected(maxDelay, local, interpolationSamples);
      m.InitSignalStacker(maxDelay);
      m.InitSelectedDelays(maxDelay, numSelected);
      m.InitObservationMatrix(order);
      m.InitEvaluation(evaluationMethod);
      m.InitFilter(forgettingFactor, average);
    end
    
    function ComputeFilteredSignal(m, recording)
      m.Prepare(recording);
      m.filteredSignal = m.filter.UpdateAndFilter(m.stackedSignal);           
    end
    
    function power = GetPower(m)
      power = m.evaluation.Evaluate(m.filteredSignal, m.stackedSignal);
    end
    
    function signal = GetSignal(m)
      signal = m.filteredSignal;
    end
  end
  
  methods (Access = private)        
    function maxDelay = ComputeMaxDelay(m, interpolationSamples)
      interpolationMargin = interpolationSamples / 2;
      maxAbsoluteDelay = max(abs(m.delays(:)));
      maxDelay = ceil(maxAbsoluteDelay + interpolationMargin);
    end
    
    function numSelected = ComputeNumSelected(~, maxDelay, local, interpolationSamples)
      if local
        numSelected = interpolationSamples;
      else
        numDelays = 2 * maxDelay + 1;
        numSelected = numDelays;
      end
    end
    
    function InitSignalStacker(m, maxDelay)
      m.signalStacker = SignalStacker(maxDelay);
    end
        
    function InitSelectedDelays(m, maxDelay, numSelected)
      m.selectedDelays = Delays(m.delays, maxDelay, numSelected);      
    end
    
    function InitObservationMatrix(m, order)      
      m.observationMatrix = ObservationMatrix(m.selectedDelays, order);
    end
    
    function InitEvaluation(m, evaluationMethod)
      m.evaluation = Evaluation.FromString(evaluationMethod, m.observationMatrix);
    end
    
    function InitFilter(m, forgettingFactor, average)
      isMinimumVariance = forgettingFactor > 0;
      discreteEvaluation = m.evaluation.IsDiscreteEvaluationPossible();
      if average
        if isMinimumVariance
          m.filter = MinimumVarianceAverageFilter(m.observationMatrix, m.selectedDelays, ...
             discreteEvaluation, forgettingFactor);
        else
          m.filter = AverageFilter(m.observationMatrix, m.selectedDelays, ...
            discreteEvaluation);          
        end
      else
        if isMinimumVariance
          m.filter = MinimumVarianceStandardFilter(m.observationMatrix, m.selectedDelays, ...
             discreteEvaluation, forgettingFactor);
        else
          m.filter = StandardFilter(m.observationMatrix, m.selectedDelays, ...
            discreteEvaluation);          
        end
      end
    end
    
    function Prepare(m, recording)      
      m.stackedSignal = m.signalStacker.StackSignal(recording);
    end
    
  end
end

