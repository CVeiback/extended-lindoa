classdef ObservationMatrix < handle
  
  properties (SetAccess = private)
    order
    selectedDelays
    
    observationMatrix
  end
  
  methods
    function m = ObservationMatrix(selectedDelays, order)
      m.selectedDelays = selectedDelays;
      m.order = order;
      
      m.PrepareObservationMatrix();
    end
    
    function observationMatrix = GetFull(m)
      observationMatrix = reshape(m.observationMatrix, m.NumStates(), [], m.NumLocations());
    end
    
    function observationMatrix = GetFullForLocation(m, location)
      observationMatrix = m.observationMatrix(:, :, :, location);
      observationMatrix = reshape(observationMatrix, m.NumStates(), []);      
    end    
    
    function observationMatrix = GetForElement(m, element, location)
      observationMatrix = m.observationMatrix(:, :, element, location);
    end
        
    function predictedSignal = Predict(m, states)
      predictedSignal = pagemtimes(states, m.GetFull());
    end
    
    function predictionError = Compare(m, state, observed)
        numLocations = size(state, 3);
        predictionError = zeros(1, numLocations);  
        predictedSignal = m.Predict(state);
        
        for location = 1:numLocations
          error = observed(:,m.selectedDelays.Stacked(location)) - predictedSignal(:,:,location);
          predictionError(location) = sumsqr(error);  
        end
    end
    
    function numLocations = NumLocations(m)
      numLocations = m.selectedDelays.NumLocations();
    end
    
    function numElements = NumElements(m)
      numElements = m.selectedDelays.NumElements();
    end
    
    function numStates = NumStates(m)
      numStates = m.order + 1;
    end
    
    function numDelays = NumDelays(m)
      numDelays = m.selectedDelays.NumDelays();
    end
    
    function numSelected = NumSelected(m)
      numSelected = m.selectedDelays.NumSelected();
    end
  end
  
  methods (Access = private)    
    function PrepareObservationMatrix(m)      
      m.observationMatrix = zeros(m.NumStates(), m.NumSelected(), m.NumElements(), m.NumLocations());            
      for location = 1:m.NumLocations()
        for element = 1:m.NumElements()
          m.PrepareSingleObservationMatrix(element, location);
        end
      end  
    end
    
    function PrepareSingleObservationMatrix(m, element, location)
      singleDelays = m.selectedDelays.SelectedDelays(element, location);
      H = m.ComputeObservationMatrixFromDelays(singleDelays);      
      m.observationMatrix(:,:, element, location) = H';
    end
    
    function observationMatrix = ComputeObservationMatrixFromDelays(m, delays)
      orders = 0:m.order;
      observationMatrix = delays' .^ orders ./ factorial(orders);
    end
  end
end

