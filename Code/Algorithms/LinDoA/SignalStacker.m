classdef SignalStacker < handle 
  properties (SetAccess = private)
    maxDelay
  end
  
  methods
    function m = SignalStacker(maxDelay)
      m.maxDelay = maxDelay;
    end
    
    function output = StackSignal(m, input)
      numSamples = size(input, 1);
      numElements = size(input, 2);
      numDelays = 2 * m.maxDelay + 1;

      output = zeros(numSamples + numDelays, numElements * numDelays);
      for delay = 1:numDelays
        rowIndices = delay - 1 + (1:numSamples);
        columnIndices = delay + (0:numElements - 1) * numDelays;
        output(rowIndices, columnIndices) = input;
      end
      output = output(m.maxDelay + (1:numSamples), :);
    end
  end
end

