classdef MeasurementError < Evaluation
  properties (SetAccess = private)
    observationMatrix
  end
  
  methods    
    function m = MeasurementError(observationMatrix)
      m.observationMatrix = observationMatrix;
    end
    
    function response = Evaluate(m, estimate, observed)
        predictionError = m.observationMatrix.Compare(estimate, observed);
        response = -predictionError; 
    end
  end
end

