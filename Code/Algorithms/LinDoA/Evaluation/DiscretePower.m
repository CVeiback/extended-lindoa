classdef DiscretePower < Evaluation
  methods        
    function response = IsDiscreteEvaluationPossible(~)
      response = true;
    end
    
    function response = Evaluate(~, estimated, ~)
      response = sum(estimated.^2);
    end
  end
end

