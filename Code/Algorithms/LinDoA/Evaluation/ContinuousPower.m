classdef ContinuousPower < Evaluation
  properties (SetAccess = private)
    powerMatrix
  end
  
  methods    
    function m = ContinuousPower(observationMatrix)
      m.InitPowerMatrix(observationMatrix.NumStates());
    end
    
    function response = Evaluate(m, estimate, ~)
      response = squeeze(sum(pagemtimes(estimate, m.powerMatrix) .* estimate,[1 2]))';
    end
  end
  
  methods (Access = private)
    function InitPowerMatrix(m, numStates)
      degrees = 1:numStates;
      tau = 1/2;
      C = hankel(1:2*numStates - 1); 
      C = C(degrees, degrees);
      m.powerMatrix = tau.^C ./ C - (-tau).^C ./ C;
    end
  end
end

