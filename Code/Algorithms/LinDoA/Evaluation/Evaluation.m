classdef Evaluation < handle
  
  methods
    function response = IsDiscreteEvaluationPossible(~)
      response = false;
    end
  end
  
  methods (Static)
    function evaluation = FromString(method, observationMatrix)
      if strcmpi(method, 'discretepower')
        evaluation = DiscretePower();
      elseif strcmpi(method, 'continuouspower')
        evaluation = ContinuousPower(observationMatrix);
      elseif strcmpi(method, 'error')
        evaluation = MeasurementError(observationMatrix);
      else
        error(['Evaluation method ''' method ''' is not recognized.']);
      end  
    end
  end
  
  methods (Abstract)
    response = Evaluate(m, estimated, observed)
  end
end

