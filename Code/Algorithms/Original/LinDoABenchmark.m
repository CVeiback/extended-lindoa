classdef LinDoABenchmark < Algorithm
  
  properties (SetAccess = private)
    order
    forgettingFactor
    local
    interpolationSamples
    minimumVariance
    average
    evaluation
    
    information
    R
    maxDelay
    observationMatrix
    fastObservationMatrix
    intermediateFilterMatrix;
    intermediateObservationMatrix;
    powerMatrix
    filterMatrix
    fastFilterMatrix
    selection
    
    filteredSignal
    power
  end
  
  methods
    function m = LinDoABenchmark (array, locations, order, forgettingFactor, ...
        local, interpolationSamples, average, evaluation)
      if nargin < 4 || isempty(forgettingFactor)
        forgettingFactor = 0.1;
      end
      if nargin < 5 || isempty(local)
        local = true;
      end
      if nargin < 6 || isempty(interpolationSamples)
        interpolationSamples = 4;
      end
      if nargin < 7 || isempty(average)
        average = false;
      end
      if nargin < 8 || isempty(evaluation)
        evaluation = 'discretepower';
      end
      
      if forgettingFactor > 0
        minimumVariance = 'mpdr';
      else
        minimumVariance = 'none';
      end
      
      m@Algorithm(array, locations);
      m.order = order;
      m.forgettingFactor = forgettingFactor;
      m.local = local;
      m.interpolationSamples = interpolationSamples;
      m.minimumVariance = minimumVariance;
      m.average = average;
      m.evaluation = evaluation;
    end
    
    function Init(m)
      if ~isempty(m.filterMatrix); return; end
      
      Ndoa = length(m.locations);
      Nchannels = m.array.NumElements();
      orders = 0:m.order;
      Nstates = length(orders);
      
      speedOfSound = 343;
      sensorPositions = m.array.position;
      directionsOfArrival = [m.locations.location];
      
      directionVector = [cos(directionsOfArrival); ...
        sin(directionsOfArrival)];
      delaysInSeconds = sensorPositions' * directionVector / speedOfSound;
      delaysInSamples = delaysInSeconds * m.array.samplingFrequency;

      Ninterpolation = m.interpolationSamples;
      maxDelay = ceil(max(abs(delaysInSamples(:))) + Ninterpolation / 2);
      Ndelays = 2 * maxDelay + 1;

      observationMatrix = zeros(Nstates, Nchannels * Ndelays, Ndoa);
      if m.local
        fastObservationMatrix = zeros(Nstates, Nchannels * Ninterpolation, Ndoa);
      else
        fastObservationMatrix = zeros(Nstates, Nchannels * Ndelays, Ndoa);    
      end
      filterMatrix = zeros(Nchannels * Ndelays, Nstates, Ndoa);
      selection = false(Nchannels * Ndelays, Ndoa);

      delaysInSamples = reshape(delaysInSamples, Nchannels, 1, Ndoa);

      extendedDelays = delaysInSamples + repmat(-(-maxDelay:maxDelay),[1 1 Ndoa]);

      for doaIndex = 1:Ndoa
        filled = false(Ndelays * Nchannels, 1);
        for channel = 1:Nchannels
          [~, localIndex] = sort(abs(extendedDelays(channel,:,doaIndex)));
          if m.local
            localIndex = localIndex(1:Ninterpolation);
          end
          globalIndex = localIndex + (channel-1)*Ndelays;

          localLogic = false(Ndelays, 1);
          localLogic(localIndex) = true;
          globalLogic = false(Ndelays * Nchannels, 1);
          globalLogic(globalIndex) = true;
          filled(globalLogic) = true;

          H = extendedDelays(channel, localLogic, doaIndex)' .^ orders ./ factorial(orders);

          observationMatrix(:, globalLogic, doaIndex) = H';
          if m.local
            fastObservationMatrix(:, (channel-1)* Ninterpolation + (1:Ninterpolation),doaIndex) = H';
          end
          filterMatrix(globalLogic, :, doaIndex) = pinv(H') / Nchannels;
        end
        if ~m.average
          filterMatrix(filled, :, doaIndex) = pinv(observationMatrix(:, filled, doaIndex));
        end
        selection(:, doaIndex) = filled;
      end     

      m.information = pagemtimes(observationMatrix, pagetranspose(observationMatrix)); % Correct for non-average
      m.R = eye(Ndelays * Nchannels);
      m.maxDelay = maxDelay;
      m.observationMatrix = observationMatrix;
      if m.local
        m.fastObservationMatrix = fastObservationMatrix;
      else
        m.fastObservationMatrix = observationMatrix;
      end

      if m.average && strcmpi(m.minimumVariance, 'mpdr')
        intermediateObservationMatrix = repmat(kron(eye(Nchannels), ones(Nstates,Ninterpolation)),[1 1 Ndoa]);
        for channel = 1:Nchannels
          channelIndex = (channel-1) * Ninterpolation + (1:Ninterpolation);
          stateIndex = (channel-1) * Nstates + (1:Nstates);
          intermediateObservationMatrix(stateIndex,channelIndex,:) = fastObservationMatrix(:,channelIndex,:);
        end

        u = sparse(Nstates, 1); u(1) = 1;
        ue = kron(speye(Nchannels),u);
        intermediateFilterMatrix = zeros(Ninterpolation * Nchannels, Nchannels, Ndoa);
        for doaIndex = 1:Ndoa
          H = intermediateObservationMatrix(:,:,doaIndex);
          intermediateFilterMatrix(:,:,doaIndex) = H' * ((H * H') \ ue);
        end

        m.intermediateFilterMatrix = intermediateFilterMatrix;
        m.intermediateObservationMatrix = intermediateObservationMatrix;
      end

      tau = 1/2;
      C = hankel(1:2*m.order+1); 
      C = C(orders+1,orders+1);
      m.powerMatrix = tau.^C./C - (-tau).^C./C;

      m.filterMatrix = filterMatrix;
      m.fastFilterMatrix = reshape(filterMatrix(:,1,:), [], Ndoa);
      m.selection = selection;
    end
    
function UpdateState(m)  
  if ~m.average
    filterMatrix = m.filterMatrix;
    Ndoa = length(m.locations);
    observationMatrix = m.observationMatrix;
    selection = m.selection;
    information = m.information;
    for doaIndex = 1:Ndoa
      filled = selection(:, doaIndex);
      H = observationMatrix(:, filled, doaIndex);
      RH = m.R(filled, filled) \ H';
      HRH = H * RH;
      filterMatrix(filled, :, doaIndex) = RH / HRH;
      information(:,:, doaIndex) = HRH;
    end
    
    m.information = information;
    m.filterMatrix = filterMatrix;
    m.fastFilterMatrix = reshape(filterMatrix(:,1,:), [], Ndoa);
  else
    if strcmpi(m.evaluation, 'discretepower')
      Ndoa = length(m.locations);
      Nchannels = m.array.NumElements();
      
      fastFilterMatrix = m.fastFilterMatrix;
      intermediateFilterMatrix = m.intermediateFilterMatrix;
      
      selection = m.selection;
      one = ones(Nchannels, 1);
      for doaIndex = 1:Ndoa
        filled = selection(:, doaIndex);
        R = m.R(filled, filled);
        
        V = sparse(intermediateFilterMatrix(:,:,doaIndex));
        
        RH = (V' * R * V) \ one; 
        w = RH / (one' * RH);
        fastFilterMatrix(filled, doaIndex) = V * w;
      end
      m.fastFilterMatrix = fastFilterMatrix;
    else
      error('Filter matrix for error computation has not been implemented yet');
      
    end
  end
end
    
    function ComputeFilteredSignal(m, recording)
      m.Init();
      
      %% Signal Preparation
      Nsamples = size(recording,1);
      Ndelays = 2 * m.maxDelay + 1;
      Nchannels = m.array.NumElements();

      shiftedSignal = zeros(Nsamples + Ndelays, Nchannels * Ndelays);
      for delay = 1: Ndelays
          shiftedSignal( delay-1 + (1:Nsamples), delay + (0:Nchannels-1)*Ndelays) = recording;
      end
      shiftedSignal = shiftedSignal(m.maxDelay + (1:Nsamples),:);

      %% Compute Covariance and Estimator
      if strcmpi(m.minimumVariance, 'mpdr')
        m.R = (1-m.forgettingFactor) * m.R + m.forgettingFactor * (shiftedSignal' * shiftedSignal) / Nsamples;
        m.UpdateState();
      end

      %% Estimate Signal
      Ndoa = length(m.locations);

      if strcmpi(m.evaluation, 'discretepower')
        filteredSignal = shiftedSignal * m.fastFilterMatrix;  
        power = sum(filteredSignal.^2);
      elseif strcmpi(m.evaluation, 'continuouspower')
        filteredSignal = pagemtimes(shiftedSignal, m.filterMatrix);  
        power = squeeze(sum(pagemtimes(filteredSignal, m.powerMatrix) .* filteredSignal,[1 2]))';
      else
        filteredSignal = pagemtimes(shiftedSignal, m.filterMatrix);

        % This computes \|y - H xhat\|^2_R for mv xhat
      %   normalizedSignal = pagemtimes(filteredSignal, state.information);
      %   predictionErrorFast = squeeze(sum(normalizedSignal .* filteredSignal, [1 2]))';
      %   selection = state.selection;
      %   R = state.R;
      %   for doaIndex = 1:Ndoa
      %     y = shiftedSignal(:,selection(:,doaIndex)) ;
      %     yR = y / chol(R(selection(:,doaIndex),selection(:,doaIndex)));
      %     yRy(doaIndex) = sumsqr(yR);
      %   end
      %   power = normalize(1 ./ (yRy - predictionErrorFast));

        % General method
        predictionError = zeros(1, Ndoa);  
        selection = m.selection;
        predictedSignal = pagemtimes(filteredSignal, m.fastObservationMatrix);

        %R = state.R;
        for doaIndex = 1:Ndoa
          error = shiftedSignal(:,selection(:,doaIndex)) - predictedSignal(:,:,doaIndex);
          %error = error / chol(R(selection(:,doaIndex),selection(:,doaIndex)));
          predictionError(doaIndex) = sumsqr(error);  
        end
        power = -predictionError;   
      end    
      m.power = power;
      m.filteredSignal = filteredSignal;
    end
    
    function power = GetPower(m)
      power = m.power;
    end
    
    function signal = GetSignal(~)
      signal = 0;
    end
  end
end

