classdef SRPBenchmark < Algorithm
  
  properties (SetAccess = private)
    complexPhaseDelays_
    forgettingFactor
    signal
  end
  
  methods
    function m = SRPBenchmark(array, locations, forgettingFactor)
      if nargin < 3 || isempty(forgettingFactor)
        forgettingFactor = 0.1;
      end
      
      m@Algorithm(array, locations);
      m.forgettingFactor = forgettingFactor;
    end
    
    function ComputeFilteredSignal(m, recording)
      speedOfSound = 343;
      micPositions = m.array.position;
      directionsOfArrival = [m.locations.location];
      samplingFrequency = m.array.samplingFrequency;
      
      [~, Nchannels] = size(recording);
      Ndimensions = size(micPositions,1);
      Ndoa = length(directionsOfArrival);

      recording = normalize(recording);

      fourierSignal = fft(recording);

      Nfft = size(fourierSignal, 1);
      NnonNegativeFFT = floor(Nfft / 2) + 1;
      fourierSignal = fourierSignal(1:NnonNegativeFFT, :);
      frequenciesInRadians = 2 * pi * (0:NnonNegativeFFT - 1)' / Nfft;

      if isempty(m.complexPhaseDelays_)
        directionVector = [cos(directionsOfArrival); sin(directionsOfArrival); zeros(Ndimensions - 2, Ndoa)];
        delaysInSeconds = micPositions' * directionVector / speedOfSound;
        delaysInSamples = delaysInSeconds * samplingFrequency;
        delaysInPhases = reshape(frequenciesInRadians, 1, 1, []) .* delaysInSamples;

        complexPhaseDelays = exp(1i * delaysInPhases);

        m.complexPhaseDelays_ = complexPhaseDelays;
      else
        complexPhaseDelays = m.complexPhaseDelays_;
      end

      filteredFourierSignal = zeros(NnonNegativeFFT, Ndoa);  
      for bin = 1:NnonNegativeFFT    
        currentFourierSignal = fourierSignal(bin,:).';
        
        filterWeights = complexPhaseDelays(:,:,bin);
        filterScale = Nchannels;
        filterWeights = filterWeights ./ filterScale;

        filteredFourierSignal(bin,:) = currentFourierSignal.' * conj(filterWeights);
      end
      
      m.signal = filteredFourierSignal;
    end    
    
    function power = GetPower(m)
      Nsamples = size(m.signal, 1)* 2 - 2;
      powerPerBin = abs(m.signal).^2;            
      positivePower = sum(powerPerBin, 1);      
      negativePower = sum(powerPerBin(2:end-1,:), 1);
      power = positivePower + negativePower;
      power = power / Nsamples^2;
    end
    
    function signal = GetSignal(~)
      filteredFourierSignal = [m.signal ; flipud(conj(m.signal(2:end-1,:)))];
      signal = ifft(filteredFourierSignal, 'symmetric');
    end
  end
end

