classdef Algorithm < handle
  
  properties (SetAccess = private)
    array
    locations
    delays
  end
  
  methods
    function m = Algorithm(array, locations)
      m.array = array;
      m.locations = locations;
      m.delays = array.ComputeDelaysInSamples(locations);
    end
    
    function power = ComputePower(m, recording)
      m.ComputeFilteredSignal(recording);
      power = m.GetPower();
    end    
    
    function signal = ComputeSignal(m, recording)
      m.ComputeFilteredSignal(recording);
      signal = m.GetSignal();
    end
  end
  methods (Abstract)
    ComputeFilteredSignal(recording)
    power = GetPower()
    signal = GetSignal()
  end
end

