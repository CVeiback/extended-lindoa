classdef FrameProcessor < handle
  
  properties
    frameSize
    stepLength
    window
    showProgress
  end
  
  methods
    function m = FrameProcessor(frameSize, stepLength, window, showProgress)
      if nargin < 1 || isempty(frameSize)
        frameSize = 1024;
      end
      if nargin < 2 || isempty(stepLength)
        stepLength = floor(frameSize / 2);
      end
      if nargin < 3 || isempty(window)
        window = hamming(frameSize, 'periodic');
      end
      if nargin < 4 || isempty(showProgress)
        showProgress = true;
      end
      
      m.frameSize = frameSize;
      m.stepLength = stepLength;
      m.window = window;      
      m.showProgress = showProgress;
    end
    
    function response = ComputePower(m, recording, algorithm)
      numSamples = size(recording, 1);
      numFrames = floor((numSamples - m.frameSize) / m.stepLength);
         
      power = [];
      for frame = 1:numFrames
        m.DisplayProgress(frame, numFrames, class(algorithm));
        
        frameOffset = (frame - 1) * m.stepLength;
        frameIndex = frameOffset + (1:m.frameSize);
        windowedFrame = recording(frameIndex,:) .* m.window;
        
        power(:, end+1) = algorithm.ComputePower(windowedFrame);
      end
      
      frameTimes = m.ComputeTimes(numFrames, algorithm.array.samplingFrequency);
      response = Response(power, algorithm, frameTimes);
    end
    
    function signal = ComputeSignal(m, recording, algorithm)
      numSamples = size(recording, 1);
      numFrames = floor((numSamples - m.frameSize) / m.stepLength);
      signal = zeros(numSamples, length(algorithm.locations));
      
      for frame = 1:numFrames
        m.DisplayProgress(frame, numFrames, class(algorithm));
        
        frameOffset = (frame - 1) * m.stepLength;
        frameIndex = frameOffset + (1:m.frameSize);
        windowedFrame = recording(frameIndex,:) .* m.window;
        
        signal(frameIndex,:) = signal(frameIndex,:) + ...
          algorithm.ComputeSignal(windowedFrame);
      end
    end
    
    function times = ComputeTimes(m, numFrames, samplingFrequency)
      frameCenterOffset = m.frameSize / 2;
      frameStarts = m.stepLength * (0:numFrames-1);
      frameCenters = frameStarts + frameCenterOffset;
      times = frameCenters / samplingFrequency;
    end
    
    function DisplayProgress(m,progress, totalProgress, label)
      if nargin < 3
        label = [];
      end
      
      currentPercentage = round(progress / totalProgress * 100);
      previousPercentage = round((progress-1) / totalProgress * 100);
      updatedPercentage = currentPercentage > previousPercentage;
      if updatedPercentage && m.showProgress
        clc;
        disp([label '   ' num2str(currentPercentage) '%'])
      end
    end
  end
end

