classdef FrequencySignal < matlab.mixin.Copyable
  
  properties (SetAccess = private)
    signal
    frequencies
  end
  
  methods
    function m = FrequencySignal(signal)      
      m.OneSidedTransform(signal);
      m.ComputeFrequencies(signal);
    end
            
    function signal = GetSignal(m)
      signal = m.signal;
    end
            
    function SetSignal(m, signal)
      assert(size(m.signal, 1) == size(signal, 1), ...
        'New signal has wrong length')
      m.signal = signal;
    end
    
    function binSignal = GetBin(m, bin)
      binSignal = m.signal(bin, :);
    end
    
    function SetBin(m, bin, binSignal)
      m.signal(bin, :) = binSignal;
    end
        
    function frequencies = GetFrequencies(m)
      frequencies = m.frequencies;
    end
    
    function numBins = NumBins(m)
      numBins = length(m.frequencies);
    end
    
    function numChannels = NumChannels(m)
      numChannels = size(m.signal, 2);
    end
    
    function numSamples = NumSamples(m)
      numSamples = (m.NumBins() - 1) * 2;
    end
        
    function Filter(m, filterTensor)
      filteredSignal = m.signal .* filterTensor;
      filteredSignal = sum(filteredSignal, 2);
      m.signal = squeeze(filteredSignal);
    end
    
    function power = Power(m)
      powerPerBin = abs(m.signal).^2;            
      positivePower = sum(powerPerBin, 1);      
      negativePower = sum(powerPerBin(2:end-1,:), 1);
      power = positivePower + negativePower;
      power = power / m.NumSamples()^2;
    end
    
    function timeSignal = InverseTransform(m)
      negativeSignal = flipud(conj(m.signal(2:end-1,:)));
      twoSidedSignal = [m.signal ; negativeSignal];
      
      % Symmetric is used here both for numerical errors and to
      % remove the imaginary component of bin N/2 if necessary.
      timeSignal = ifft(twoSidedSignal, 'symmetric');
    end
  end
  
  methods (Access = private)
    function OneSidedTransform(m, signal)
      numSamples = size(signal, 1);
      numBins = m.ComputeOneSidedBins(numSamples);
      
      m.signal = fft(signal);
      m.signal = m.signal(1:numBins, :);
    end
    
    function numOneSidedBins = ComputeOneSidedBins(~, numSamples)
      numOneSidedBins = floor(numSamples / 2) + 1;
    end
    
    function ComputeFrequencies(m, signal)
      numSamples = size(signal, 1);
      numBins = m.ComputeOneSidedBins(numSamples);
      m.frequencies = 2 * pi * (0:numBins - 1)' / numSamples;
    end
  end
end

