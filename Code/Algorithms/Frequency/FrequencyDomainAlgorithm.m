classdef FrequencyDomainAlgorithm < Algorithm
  
  properties (SetAccess = protected)
    frequencySignal
    steeringVectors
    filterWeights
    filteredSignal
  end
  
  methods
    function m = FrequencyDomainAlgorithm(array, locations)
      m@Algorithm(array, locations);
    end
    
    function ComputeFilteredSignal(m, recording)
      m.Prepare(recording);
      m.InitIfFirst();      
      m.UpdateFilterWeights();
      m.FilterSignal();
    end
    
    function power = GetPower(m)
      power = m.filteredSignal.Power();
    end
    
    function signal = GetSignal(m)
      signal = m.filteredSignal.InverseTransform();
    end  
  end
  
  methods (Access = protected)
    function Prepare(m, recording)
      recording = m.PreProcess(recording);      
      m.frequencySignal = FrequencySignal(recording);      
      m.PostProcess();
    end
    
    function recording = PreProcess(~, recording)
      
    end
    
    function PostProcess(~)
      
    end
    
    function InitIfFirst(m)
      if isempty(m.steeringVectors)
        m.Init();
      end
    end
    
    function Init(m) 
        m.InitSteeringVectors();
        m.InitFilterWeights();
    end
    
    function InitSteeringVectors(m)
      frequencies = m.frequencySignal.GetFrequencies();
      delaysInPhases = reshape(frequencies, 1 , 1, []) .* m.delays;        
      m.steeringVectors = exp(1i * delaysInPhases);
      
      % For beamforming, the last bin must be real:
      %  Option 1: Use exp(1i*pi*tau) anyway, and remove imaginary
      %  component before ifft: Correct magnitude in frequency domain, same
      %    as option 2 in time domain.
      %  Option 2: Use cos(pi*tau): Wrong magnitude, lose energy, good
      %    performance
      % m.steeringVectors(:,:,end) = real(m.steeringVectors(:,:,end));
      %  Option 3: Use 1 if real(cos(pi*tau)) > 0, otherwise 1: Correct
      %    magnitude and energy
    end
    
    function InitFilterWeights(m)
      m.filterWeights = zeros(size(m.steeringVectors));
    end
    
    function UpdateFilterWeights(~)

    end
    
    function FilterSignal(m)
      m.filteredSignal = m.frequencySignal.copy();
      tensorWeights = permute(m.filterWeights, [3 1 2]);
      m.filteredSignal.Filter(conj(tensorWeights));
    end 
  end
end

