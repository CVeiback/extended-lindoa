classdef SteeredResponsePower < FrequencyDomainAlgorithm
    
  methods
    function m = SteeredResponsePower(array, locations)
      m@FrequencyDomainAlgorithm(array, locations);      
    end
  end
  
  methods (Access = protected)
    function recording = PreProcess(~, recording)
      recording = normalize(recording);
    end
    
    function InitFilterWeights(m)
      numChannels = m.frequencySignal.NumChannels();
      m.filterWeights = m.steeringVectors / numChannels;
    end
  end
end

