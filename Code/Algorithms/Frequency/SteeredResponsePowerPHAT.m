classdef SteeredResponsePowerPHAT < FrequencyDomainAlgorithm
    
  methods
    function m = SteeredResponsePowerPHAT(array, locations)
      m@FrequencyDomainAlgorithm(array, locations);      
    end
  end
  
  methods (Access = protected)
    function recording = PreProcess(~, recording)
      recording = normalize(recording);
    end
    
    function InitFilterWeights(m)
      numChannels = m.frequencySignal.NumChannels();
      m.filterWeights = m.steeringVectors / numChannels;
    end
    
    function PostProcess(m)
      phase = angle(m.frequencySignal.GetSignal());
      m.frequencySignal.SetSignal(exp(1i * phase));
    end
  end
end