classdef MinimumVarianceDistortionlessResponse < FrequencyDomainAlgorithm
  
  properties (SetAccess = protected)
    forgettingFactor
    initialCovariance
    covariance
  end
  
  methods
    function m = MinimumVarianceDistortionlessResponse(array, locations, forgettingFactor)
      if nargin < 3 || isempty(forgettingFactor)
        forgettingFactor = 0.1;
      end
      
      m@FrequencyDomainAlgorithm(array, locations);
      m.forgettingFactor = forgettingFactor;
      m.initialCovariance = eye(array.NumElements());
    end       
  end
  
  methods (Access = protected)    
    
    function recording = PreProcess(~, recording)
      recording = normalize(recording);
    end
    
    function InitFilterWeights(m)
      InitFilterWeights@FrequencyDomainAlgorithm(m);
      m.covariance = repmat(m.initialCovariance, 1, 1, m.NumBins());
    end
    
    function numBins = NumBins(m)
      numBins = m.frequencySignal.NumBins();
    end
    
    function UpdateFilterWeights(m)
      for bin = 1:m.NumBins()
        m.UpdateCovarianceForBin(bin);
        m.UpdateFilterWeightsForBin(bin);
      end
    end
     
    function UpdateCovarianceForBin(m, bin)      
      binSignal = m.frequencySignal.GetBin(bin).';
      covarianceUpdate = binSignal * binSignal';
      m.covariance(:,:,bin) = m.forgettingFactor * covarianceUpdate + ...
        m.covariance(:,:,bin) * (1 - m.forgettingFactor);    
    end
    
    function UpdateFilterWeightsForBin(m, bin)
      unscaledWeights = m.covariance(:,:,bin) \ m.steeringVectors(:,:,bin);
      scaleFactor = sum(real(conj(m.steeringVectors(:,:,bin)) .* unscaledWeights));
      m.filterWeights(:,:,bin) = unscaledWeights ./ scaleFactor;
    end
  end
end

