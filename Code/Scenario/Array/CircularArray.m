classdef CircularArray < Array
  
  properties (SetAccess = private)
    radius
    orientation
  end
  
  methods
    function m = CircularArray(numElements, radius, orientation, noise, samplingFrequency)
      %Construct a circular array of given radius and orientation in
      %radians
      
      if nargin < 3 || isempty(orientation)
        orientation = 0;
      end
      
      if nargin < 4 || isempty(noise)
        noise = 0.1;
      end
      
      if nargin < 5 || isempty(samplingFrequency)
        samplingFrequency = 8000;
      end

      angles = orientation + linspace(0, 2*pi, numElements + 1);
      angles(end) = [];
      
      position = radius * [cos(angles); sin(angles)];
      
      m@Array(position, noise, samplingFrequency)
      m.radius = radius;
      m.orientation = orientation;
    end
  end
end

