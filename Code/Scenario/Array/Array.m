classdef Array < handle
  
  properties (SetAccess = private)
    position
    noise
    samplingFrequency
    speedInMedium
  end
  
  methods
    function m = Array(position, noise, samplingFrequency)
      %Construct an array of given position for elements
      
      m.position = position;
      m.noise = noise;
      m.samplingFrequency = samplingFrequency;
      m.speedInMedium = 343;
    end
    
    function numElements = NumElements(m)
      numElements = size(m.position, 2);
    end
    
    function delaysInSeconds = ComputeDelays(m, location)
      % Compute delays for each element in the array fiven the source
      % location.
      
      numLocations = length(location);
      distances = zeros(m.NumElements(), numLocations);
      for ii = 1:numLocations
        distances(:,ii) = location(ii).DistanceToPosition(m.position);
      end
      delaysInSeconds = distances / m.speedInMedium;
    end
    
    function delaysInSamples = ComputeDelaysInSamples(m, location)
      delaysInSeconds = m.ComputeDelays(location);
      delaysInSamples = delaysInSeconds * m.samplingFrequency;
    end
  end
end

