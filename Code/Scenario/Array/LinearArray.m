classdef LinearArray < Array
  
  properties (SetAccess = private)
    length
    orientation
  end
  
  methods
    function m = LinearArray(numElements, length, orientation, noise, samplingFrequency)
      %Construct a linear array of given length and orientation in radians
      
      if nargin < 3 || isempty(orientation)
        orientation = 0;
      end
      
      if nargin < 4 || isempty(noise)
        noise = 0.1;
      end
      
      if nargin < 5 || isempty(samplingFrequency)
        samplingFrequency = 8000;
      end
      
      linearPosition = linspace(-length/2,length/2, numElements);
      position = [cos(orientation); sin(orientation)] .* linearPosition;
      
      m@Array(position, noise, samplingFrequency)
      m.length = length;
      m.orientation = orientation;
    end
  end
end


