classdef Scenario < handle
  
  properties
    array
    sources
  end
  
  methods
    function m = Scenario(array)
     
      m.array = array;
      m.sources = [];
    end
    
    function AddSource(m, source)
      if isempty(m.sources)
        m.sources = source;
      else
        m.sources(end+1) = source;
      end
    end
    
    function recording = Simulate(m, time)      
      recording = m.SimulateSources(time);
      recording = m.AddNoise(recording);
    end
    
    function recording = SimulateSources(m, time)      
      recording = m.sources(1).Sample(m.array, time);      
      for ii = 2:m.NumSources()
        recording = recording + m.sources(ii).Sample(m.array, time);
      end      
    end
    
    function recording = CleanSources(m, time)      
      recording = m.sources(1).CleanSample(m.array, time);      
      for ii = 2:m.NumSources()
        recording(ii, :) = m.sources(ii).CleanSample(m.array, time);
      end      
    end
    
    function recording = AddNoise(m, recording)
      recording = recording + m.array.noise * randn(size(recording));
    end
    
    function numSources = NumSources(m)
      numSources = length(m.sources);
    end
  end
end

