classdef Location
  
  properties (SetAccess = private)
    location
  end
  
  methods
    function m = Location(location)
      m.location = location;
    end      
  end
  
  methods (Abstract)
    distance = DistanceToPosition(position)
  end
end

