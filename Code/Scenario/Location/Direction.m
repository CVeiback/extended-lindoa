classdef Direction < Location
    
  methods
    function m = Direction(direction)
      m@Location(direction);
    end
    
    function distance = DistanceToPosition(m, position)
      directionVector = [cos(m.location); sin(m.location)];
      distance = position' * directionVector;
    end
  end
  
  methods (Static)
    function array = Generate(directions)
      array = [];
      for ii = 1:size(directions, 2)
        if isempty(array)
          array = Direction(directions(:, ii));
        else
          array(end+1) = Direction(directions(:,ii));
        end
      end
    end
  end
end

