classdef Source < handle
  
  properties (SetAccess = private)
    signal
    location
  end
  
  methods
    function m = Source(signal, location)
      m.signal = signal;
      m.location = location;
    end
    
    function samples = Sample(m, array, time)
      delays = array.ComputeDelays(m.location);
      samples = m.signal.Sample(array.samplingFrequency, time, delays);
      samples = normalize(samples')';
    end
    
    function samples = CleanSample(m, array, time)
      samples = m.signal.Sample(array.samplingFrequency, time, 0);
      samples = normalize(samples')';
    end
  end
end

