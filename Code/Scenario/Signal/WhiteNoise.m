classdef WhiteNoise < Signal
  
  properties
    id
  end
  
  methods    
    function m = WhiteNoise(id)
      m.id = id;
    end
    
    function signal = Sample(m, samplingFrequency, time, offset)
      rng(1000 + m.id)
      
      padding = 1;
      originalIndex = 1:samplingFrequency * (time + 2 * padding);
      originalTimes = (originalIndex - 1) / samplingFrequency - padding;
      originalSignal = randn(samplingFrequency * (time+2), 1);
      continuousSignal = griddedInterpolant(originalTimes, originalSignal, 'spline');
      
      samplingTimes = (0:samplingFrequency*(time)-1) / samplingFrequency;      
      signal = continuousSignal(samplingTimes + offset);
    end
  end
end

