classdef Signal < handle
  methods (Abstract)
    signal = Sample(samplingFrequency, time, offset)
  end
end

