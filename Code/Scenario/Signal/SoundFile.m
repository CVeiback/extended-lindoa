classdef SoundFile < Signal
  
  properties
    id
    files
    padding = 1;
  end
  
  methods    
    function m = SoundFile(id)
      files = {'handel', 'laughter', 'train', 'gong'};
      numFiles = length(files);
      
      if id > numFiles
        error(['Max id is ' num2str(numFiles) '.']);
      end       
        
      m.files = files;
      m.id = id;      
    end
    
    function signal = Sample(m, samplingFrequency, time, offset)
      [paddedSignal, paddedTime] = m.PrepareSignal(samplingFrequency, time);
      continuousSignal = griddedInterpolant(paddedTime, paddedSignal, 'spline');      
      samplingTimes = m.ComputeTimestamps(samplingFrequency, time);
      signal = continuousSignal(samplingTimes + offset);
    end
  end
  
  methods (Access = private)
    
    function [signal, timestamps] = PrepareSignal(m, samplingFrequency, time)
      rawSignal = m.ReadSignal(samplingFrequency);      
      paddedTime = time + 2 * m.padding;      
      signal = m.PadSignal(rawSignal, samplingFrequency, paddedTime);      
      timestamps = m.ComputeTimestamps(samplingFrequency, paddedTime) - m.padding;
    end
    
    function signal = ReadSignal(m, samplingFrequency)
      data = load(m.files{m.id});
      signal = resample(data.y, samplingFrequency, data.Fs);
    end
    
    function paddedSignal = PadSignal(~, signal, samplingFrequency, time)
      numRawSamples = length(signal);
      numSamples = samplingFrequency * time;      
      paddedIndex = 1:numSamples;
      
      rawIndex = mod(paddedIndex - 1, numRawSamples) + 1;
      paddedSignal = signal(rawIndex);
    end
    
    function timestamps = ComputeTimestamps(~, samplingFrequency, time)
      numSamples = samplingFrequency * time;
      sampleIndex = 1:numSamples;
      timestamps = (sampleIndex - 1) / samplingFrequency;
    end
  end
end

