close all
clearvars

%% Setup
addpath(genpath('.'));

%% Setup Scenario
time = 2;
[scenario, array] = CreateExperimentScenario();
recording = scenario.Simulate(time);
[directions, frameProcessor] = SetupEstimationFramework();

%% Setup the Estimators for DoA Estimation
srp = SteeredResponsePower(array, directions); 
srpphat = SteeredResponsePowerPHAT(array, directions);
mvdr = MinimumVarianceDistortionlessResponse(array, directions);
lindoa = LinDoA(array, directions, 5, 0);
mvlindoa = LinDoA(array, directions, 5, 0.1);
alindoa = LinDoA(array, directions, 5, 0, [], [], true);
mvalindoa = LinDoA(array, directions, 5, 0.1, [], [], true);

%% DoA Estimation using Various Methods
responseSRP = frameProcessor.ComputePower(recording', srp);
responseSRPPHAT = frameProcessor.ComputePower(recording', srpphat);
responseMVDR = frameProcessor.ComputePower(recording', mvdr);
responseLindoa = frameProcessor.ComputePower(recording', lindoa);
responseMVLindoa = frameProcessor.ComputePower(recording', mvlindoa);
responseALindoa = frameProcessor.ComputePower(recording', alindoa);
responseMVALindoa = frameProcessor.ComputePower(recording', mvalindoa);

presentation = Presentation(scenario, responseSRP, 'DS');
presentation.AddResponse(responseSRPPHAT, 'SRP-PHAT');
presentation.AddResponse(responseMVDR, 'MVDR');
presentation.AddResponse(responseLindoa, 'LinDoA');
presentation.AddResponse(responseMVLindoa, 'MV-LinDoA');
presentation.AddResponse(responseALindoa, 'A-LinDoA');
presentation.AddResponse(responseMVALindoa, 'AMV-LinDoA');

presentation.DoAImage();
presentation.EstimationError();

%% Beamforming
cleanSignals = scenario.CleanSources(time)';
beamDirection = [scenario.sources(:).location];

algorithms = {}; names = {};
algorithms{end+1} = SteeredResponsePower(array, beamDirection); names{end+1} = 'DS';
algorithms{end+1} = SteeredResponsePowerPHAT(array, beamDirection); names{end+1} = 'SRP-PHAT';
algorithms{end+1} = MinimumVarianceDistortionlessResponse(array, beamDirection); names{end+1} = 'MVDR';
algorithms{end+1} = LinDoA(array, beamDirection, 5, 0); names{end+1} = 'LinDoA';
algorithms{end+1} = LinDoA(array, beamDirection, 5, 0.1); names{end+1} = 'MV-LinDoA';
algorithms{end+1} = LinDoA(array, beamDirection, 5, 0, [], [], true); names{end+1} = 'A-LinDoA';
algorithms{end+1} = LinDoA(array, beamDirection, 5, 0.1, [], [], true); names{end+1} = 'AMV-LinDoA';
names = pad(names);

numSamples = size(recording, 2);
numSources = length(beamDirection);
numAlgorithms = length(algorithms);
signal = zeros(numSamples, numSources, numAlgorithms);
for ii = 1:numAlgorithms
  signal(:,:,ii) = frameProcessor.ComputeSignal(recording', algorithms{ii});
end

for ii = 1:numAlgorithms
  R = normalize(cleanSignals)' * normalize(signal(:,:,ii)) / numSamples;
  disp([names{ii} '   ' num2str(round([R(1,1) R(2,2) R(2,1) R(1,2)],2))])
end

%% 
function [directions, frameProcessor] = SetupEstimationFramework()
numDirections = 360;
frameSize = 1024;

directionGrid = linspace(0, 2*pi, numDirections + 1); directionGrid(end) = [];
directions = Direction.Generate(directionGrid);

frameProcessor = FrameProcessor(frameSize);
end